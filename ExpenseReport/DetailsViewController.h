//
//  DetailsViewController.h
//  ExpenseReport
//
//  Created by Lyndy Tankersley on 2/15/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>

@interface DetailsViewController : UIViewController
{
    UITextField* locTxtFld;
    UITextField* costTxtFld;
    UITextField* notesTxtFld;
    UIDatePicker *datePicker;
    UILabel *locLbl;
    UILabel *dateLbl;
    UILabel *costLbl;
    UILabel *notesLbl;
}

@property (nonatomic, strong) NSManagedObject* expense;

@end
