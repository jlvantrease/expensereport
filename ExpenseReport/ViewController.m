//
//  ViewController.m
//  ExpenseReport
//
//  Created by Lyndy Tankersley on 2/15/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    expenseTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    expenseTableView.translatesAutoresizingMaskIntoConstraints = NO;
    expenseTableView.delegate = self;
    expenseTableView.dataSource = self;
    [self.view addSubview:expenseTableView];
    
    NSDictionary* viewsDictionary = NSDictionaryOfVariableBindings(expenseTableView);
    NSDictionary *metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[expenseTableView]|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[expenseTableView]|" options:0 metrics:metrics views:viewsDictionary]];
    
    UIBarButtonItem* addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonTouched:)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [self loadData];
}

-(void)loadData{
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"ExpenseInfo"];
    NSArray* array = [[context executeFetchRequest:fetchRequest error:nil]mutableCopy];
    arrayOfReports = [array mutableCopy];
    [expenseTableView reloadData];
}

-(void)addButtonTouched:(id)sender{
    DetailsViewController* dvc = [DetailsViewController new];
    [self.navigationController pushViewController:dvc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayOfReports.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSManagedObject* object = arrayOfReports[indexPath.row];
    cell.textLabel.text = [object valueForKey:@"location"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:(NSIndexPath *) indexPath animated:YES];
    
    DetailsViewController* dvc = [DetailsViewController new];
    dvc.expense = arrayOfReports[indexPath.row];
    [self.navigationController pushViewController:dvc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
