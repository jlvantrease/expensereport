//
//  ViewController.h
//  ExpenseReport
//
//  Created by Jason Vantrease on 2/15/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailsViewController.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableView* expenseTableView;
    NSMutableArray* arrayOfReports;
}


@end

