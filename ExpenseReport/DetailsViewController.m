//
//  DetailsViewController.m
//  ExpenseReport
//
//  Created by Lyndy Tankersley on 2/15/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    locLbl =  [[UILabel alloc] init];
    locLbl.translatesAutoresizingMaskIntoConstraints = NO;
    locLbl.textColor=[UIColor blackColor];
    [self.view addSubview:locLbl];
    locLbl.text = @"Location";
    
    dateLbl =  [[UILabel alloc] init];
    dateLbl.translatesAutoresizingMaskIntoConstraints = NO;
    dateLbl.textColor=[UIColor blackColor];
    [self.view addSubview:dateLbl];
    dateLbl.text = @"Date";
    
    costLbl =  [[UILabel alloc] init];
    costLbl.translatesAutoresizingMaskIntoConstraints = NO;
    costLbl.textColor=[UIColor blackColor];
    [self.view addSubview:costLbl];
    costLbl.text = @"Cost";
    
    notesLbl =  [[UILabel alloc] init];
    notesLbl.translatesAutoresizingMaskIntoConstraints = NO;
    notesLbl.textColor=[UIColor blackColor];
    [self.view addSubview:notesLbl];
    notesLbl.text = @"Notes";
    
    locTxtFld = [UITextField new];
    locTxtFld.borderStyle = UITextBorderStyleRoundedRect;
    locTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:locTxtFld];
    
    costTxtFld = [UITextField new];
    costTxtFld.borderStyle = UITextBorderStyleRoundedRect;
    costTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:costTxtFld];
    
    notesTxtFld = [UITextField new];
    notesTxtFld.borderStyle = UITextBorderStyleRoundedRect;
    notesTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    
    datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:datePicker];
    
    [self.view addSubview:notesTxtFld];
    
    NSDictionary *viewDict = NSDictionaryOfVariableBindings(locLbl, locTxtFld,dateLbl,datePicker,costLbl,costTxtFld,notesLbl,notesTxtFld);
    NSDictionary *metrics = @{@"margin":[NSNumber numberWithInt:20], @"topmargin":[NSNumber numberWithInt:70]};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[locLbl]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[locTxtFld]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[dateLbl]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[costLbl]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[notesLbl]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[costTxtFld]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[notesTxtFld]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[datePicker]|" options:0 metrics:metrics views:viewDict]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topmargin-[locLbl]-10-[locTxtFld]-20-[dateLbl]-10-[datePicker]-20-[costLbl]-10-[costTxtFld]-20-[notesLbl]-10-[notesTxtFld]" options:0 metrics:metrics views:viewDict]];
    
    UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnTouched:)];

     UIBarButtonItem* sendBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sendBtnTouched:)];
    
    self.navigationItem.rightBarButtonItems = @[saveBtn,sendBtn];
    
    if(self.expense){
        locTxtFld.text = [self.expense valueForKey:@"location"];
        datePicker.date = [self.expense valueForKey:@"date"];
        costTxtFld.text = [self.expense valueForKey:@"cost"];
        notesTxtFld.text = [self.expense valueForKey:@"notes"];
    }
}

-(void)saveBtnTouched:(id)sender{
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    if(self.expense){
        [self.expense setValue:locTxtFld.text forKey:@"location"];
        [self.expense setValue:datePicker.date forKey:@"date"];
        [self.expense setValue:costTxtFld.text forKey:@"cost"];
        [self.expense setValue:notesTxtFld.text forKey:@"notes"];
    }else{
        NSManagedObject *newExpense = [NSEntityDescription insertNewObjectForEntityForName:@"ExpenseInfo" inManagedObjectContext:context];
        [newExpense setValue:locTxtFld.text forKey:@"location"];
        [newExpense setValue:datePicker.date forKey:@"date"];
        [newExpense setValue:costTxtFld.text forKey:@"cost"];
        [newExpense setValue:notesTxtFld.text forKey:@"notes"];
    }
    
    NSError *error = nil;
    if(![context save:&error]){
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)sendBtnTouched:(id)sender{
    MFMailComposeViewController *mailC = [[MFMailComposeViewController alloc]init];
    mailC.mailComposeDelegate = self;
    [mailC setSubject:@"Expense Report"];
    NSString *message = [NSString stringWithFormat:@"Location: %@\n\nDate: %@Cost: %@\n\nNotes: %@\n\n", locTxtFld.text, datePicker.date, costTxtFld.text, notesTxtFld.text];
    [mailC setMessageBody:message isHTML:NO];
    [self presentViewController:mailC animated:YES completion:^{
    
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
